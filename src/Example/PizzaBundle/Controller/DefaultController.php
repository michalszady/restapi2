<?php

namespace Example\PizzaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ExamplePizzaBundle:Default:index.html.twig');
    }
}
