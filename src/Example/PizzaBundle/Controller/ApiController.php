<?php

namespace Example\PizzaBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

class ApiController extends FOSRestController {

    public function optionsPizzaAction() {
        
    }

    public function getKetchupAction() {

        $data = [
            'type' => 'd',
            'quantity' => '30ml',
        ];
        $view = $this->view($data, 200);
        return $this->handleView($view);
    }

    public function getKetchupsAction($a) {

        $data = [
            'type' => $a,
            'quantity' => '30ml',
        ];
        $view = $this->view($data, 200);
        return $this->handleView($view);
    }

    

    
  
    public function postTestAction(Request $request) {
        
       
        $name = $request->get('name');
        $role = $request->get('role');
        if (empty($name) || empty($role)) {
            return new View("NULL VALUES ARE NOT ALLOWED".$name, Response::HTTP_NOT_ACCEPTABLE);
        }
        
        return new View("User Added Successfully".$name, Response::HTTP_OK);
    } //"post_dataset"  [POST] /dataset    

}
